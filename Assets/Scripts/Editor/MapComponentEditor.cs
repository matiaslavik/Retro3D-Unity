using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(MapComponent))]
[CanEditMultipleObjects]
public class MapComponentEditor : Editor 
{
    private bool isPickingTexture = false;
    private int currentPickerWindow;
    private int currentPickerTextureIndex = -1;
    private int selectedTextureIndex = -1;

    public override void OnInspectorGUI()
    {
        MapComponent mapComponent = (MapComponent)target;
        int oldDimX = mapComponent.dimX;
        int oldDimY = mapComponent.dimY;

        EditorGUI.BeginChangeCheck();

        mapComponent.defaultCubePrefab = (GameObject)EditorGUILayout.ObjectField("Cube prefab", mapComponent.defaultCubePrefab, typeof(GameObject), allowSceneObjects: false);
        mapComponent.cubeMaterial = (Material)EditorGUILayout.ObjectField("Cube material", mapComponent.cubeMaterial, typeof(Material), allowSceneObjects: false);

        mapComponent.dimX = Math.Clamp(EditorGUILayout.IntField("Dimension X", mapComponent.dimX), 0, 256);
        mapComponent.dimY = Math.Clamp(EditorGUILayout.IntField("Dimension Y", mapComponent.dimY), 0, 256);
        mapComponent.ceilingTexture = (Texture2D)EditorGUILayout.ObjectField("Ceiling texture", mapComponent.ceilingTexture, typeof(Texture2D), false);
        mapComponent.floorTexture = (Texture2D)EditorGUILayout.ObjectField("Floor texture", mapComponent.floorTexture, typeof(Texture2D), false);

        if (EditorGUI.EndChangeCheck())
            EditorUtility.SetDirty(mapComponent);

        if (mapComponent.map.Length != mapComponent.dimX * mapComponent.dimY)
        {
            int[] oldMap = new int[mapComponent.map.Length];
            Array.Copy(mapComponent.map, oldMap, mapComponent.map.Length);
            mapComponent.map = new int[mapComponent.dimX * mapComponent.dimY];
            Array.Fill(mapComponent.map, -1);
            for (int iy = 0; iy < oldDimY && iy < mapComponent.dimY; iy++)
            {
                for (int ix = 0; ix < oldDimX && ix < mapComponent.dimX; ix++)
                {
                    int oldIndex = ix + iy * oldDimX;
                    int newIndex = ix + iy * mapComponent.dimX;
                    mapComponent.map[newIndex] = oldMap[oldIndex];
                }
            }
            EditorUtility.SetDirty(mapComponent);
        }

        GUILayout.Label("Wall textures");
        selectedTextureIndex = GUILayout.SelectionGrid(selectedTextureIndex, mapComponent.textures.ToArray(), 4);

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("+", new GUILayoutOption[]{ GUILayout.ExpandWidth(false) }))
        {
            currentPickerWindow = EditorGUIUtility.GetControlID (FocusType.Passive);
            EditorGUIUtility.ShowObjectPicker<Texture2D>(null, false, "", currentPickerWindow);
            isPickingTexture = true;
        }

        if (selectedTextureIndex >= 0 && GUILayout.Button("-", new GUILayoutOption[]{ GUILayout.ExpandWidth(false) }))
        {
            mapComponent.textures.RemoveAt(selectedTextureIndex);
            EditorUtility.SetDirty(mapComponent);
        }
        GUILayout.EndHorizontal();

        string commandName = Event.current.commandName;
        if (isPickingTexture && commandName == "ObjectSelectorClosed") {
            Texture2D selectedTexture = (Texture2D)EditorGUIUtility.GetObjectPickerObject();
            if (selectedTexture)
            {
                if (selectedTexture != null && currentPickerTextureIndex == -1)
                    mapComponent.textures.Add(selectedTexture);
                else if(currentPickerTextureIndex < mapComponent.textures.Count)
                    mapComponent.textures[currentPickerTextureIndex] = selectedTexture;
                currentPickerTextureIndex = -1;
                isPickingTexture = false;
                EditorUtility.SetDirty(mapComponent);
            }
        }
    }

    void OnSceneGUI()
    {
        HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
        
        MapComponent mapComponent = (MapComponent)target;
        if (selectedTextureIndex != -1)
        {
            Ray ray = HandleUtility.GUIPointToWorldRay( Event.current.mousePosition );
            float t = -ray.origin.y / ray.direction.y;
            Vector3 intersection = ray.origin + ray.direction * t;
            int ix = Mathf.FloorToInt(intersection.x);
            int iy = Mathf.FloorToInt(intersection.z);
            if (ix >= 0 && ix < mapComponent.dimX && iy >= 0 && iy < mapComponent.dimY)
            {
                int mapIndex = ix + iy * mapComponent.dimX;
                if (Event.current.type == EventType.MouseDown || Event.current.type == EventType.MouseDrag && Event.current.button == 0)
                {
                    mapComponent.map[mapIndex] = selectedTextureIndex;
                    EditorUtility.SetDirty(mapComponent);
                    Event.current.Use();
                }
            }
        }
        Selection.objects = new UnityEngine.Object[]{mapComponent};
    }
}
