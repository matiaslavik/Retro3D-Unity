using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class MapComponent : MonoBehaviour
{
    [SerializeField]
    public GameObject defaultCubePrefab; // TODO: private
    [SerializeField]
    public Material cubeMaterial; // TODO: private
    [SerializeField]
    public List<Texture2D> textures = new List<Texture2D>();
    [SerializeField]
    public Texture2D ceilingTexture;
    [SerializeField]
    public Texture2D floorTexture;

    private Mesh cubeMesh;

    [SerializeField]
    public int[] map = new int [0]; // TODO: private
    [SerializeField]
    public int dimX; // TODO: private
    [SerializeField]
    public int dimY; // TODO: private

    void Start()
    {
        if (defaultCubePrefab)
        { 
            cubeMesh = defaultCubePrefab.GetComponent<MeshFilter>().sharedMesh;
        }
    }

    void Update()
    {
        transform.localScale = Vector3.one;
        transform.localRotation = Quaternion.identity;
        transform.position = Vector3.zero;

#if UNITY_EDITOR
        if (cubeMesh && cubeMaterial)
        {
            // Draw grid
            for (int iy = 0; iy < dimY; iy++)
            {
                Vector3 lineStart = new Vector3(0.0f, 0.0f, (float)iy);
                Vector3 lineEnd = new Vector3((float)dimX, 0.0f, (float)iy);
                Debug.DrawLine(lineStart, lineEnd, Color.black);
            }
            for (int ix = 0; ix < dimX; ix++)
            {
                Vector3 lineStart = new Vector3((float)ix, 0.0f, 0.0f);
                Vector3 lineEnd = new Vector3((float)ix, 0.0f, (float)dimY);
                Debug.DrawLine(lineStart, lineEnd, Color.black);
            }
            // Draw walls
            for (int iy = 0; iy < dimY; iy++)
            {
                for (int ix = 0; ix < dimX; ix++)
                {
                    int mapIndex = ix + iy * dimX;
                    int textureIndex = map[mapIndex];
                    if (textureIndex >= 0 && textureIndex < textures.Count)
                    {
                        cubeMaterial.SetTexture("_MainTex", textures[textureIndex]);
                        Matrix4x4 transMat = Matrix4x4.Translate(new Vector3((float)ix + 0.5f, 0.5f, (float)iy + 0.5f));
                        Graphics.DrawMeshInstanced(cubeMesh, 0, cubeMaterial, new Matrix4x4[]{ this.transform.localToWorldMatrix * transMat });
                    }
                }
            }
        }
#endif
    }
}
