using System;
using System.Runtime.InteropServices;

public class Retro3D
{
    private const string Retro3DLibrary = "Retro3D";

    [DllImport(Retro3DLibrary, CallingConvention = CallingConvention.Cdecl)]
    public static extern void TestTest();
    [DllImport(Retro3DLibrary, CallingConvention = CallingConvention.Cdecl)]
    public static extern IntPtr CreateRenderer();
    [DllImport(Retro3DLibrary, CallingConvention = CallingConvention.Cdecl)]
    public static extern void Update(IntPtr renderer, float deltaTime);
    [DllImport(Retro3DLibrary, CallingConvention = CallingConvention.Cdecl)]
    public static extern void SetCamera(IntPtr renderer, float x, float y, float dirX, float dirY);
    [DllImport(Retro3DLibrary, CallingConvention = CallingConvention.Cdecl)]
    public static extern void SetResolution(IntPtr renderer, uint width, uint height);
    [DllImport(Retro3DLibrary, CallingConvention = CallingConvention.Cdecl)]
    public static extern void SetMap(IntPtr renderer, IntPtr map, uint width, uint height);
    [DllImport(Retro3DLibrary, CallingConvention = CallingConvention.Cdecl)]
    public static extern void SetTexture(IntPtr renderer, uint index, IntPtr data, uint length, uint width, uint height);
    [DllImport(Retro3DLibrary, CallingConvention = CallingConvention.Cdecl)]
    public static extern void SetCeilingTexture(IntPtr renderer, uint index);
    [DllImport(Retro3DLibrary, CallingConvention = CallingConvention.Cdecl)]
    public static extern void SetFloorTexture(IntPtr renderer, uint index);
    [DllImport(Retro3DLibrary, CallingConvention = CallingConvention.Cdecl)]
    public static extern IntPtr GetFrameBufferPtr(IntPtr renderer);
    [DllImport(Retro3DLibrary, CallingConvention = CallingConvention.Cdecl)]
    public static extern uint GetFrameBufferSize(IntPtr renderer);
}
