using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class Retro3DRenderer : MonoBehaviour
{
    private IntPtr renderer;
    private Texture2D cameraTexture;

    private const int FRAMEBUFFER_WIDTH = 640;
    private const int FRAMEBUFFER_HEIGHT = 400;

    public MapComponent mapComponent;

    void Start()
    {
        cameraTexture = new Texture2D(FRAMEBUFFER_WIDTH, FRAMEBUFFER_HEIGHT, TextureFormat.RGBAFloat, false, true);
        
        renderer = Retro3D.CreateRenderer();

        Retro3D.SetResolution(renderer, (uint)FRAMEBUFFER_WIDTH, (uint)FRAMEBUFFER_HEIGHT);

        List<Texture2D> textures = new List<Texture2D>(mapComponent.textures);

        if (mapComponent.ceilingTexture)
        {
            textures.Add(mapComponent.ceilingTexture);
            Retro3D.SetCeilingTexture(renderer, (uint)(textures.Count - 1));
        }
        if (mapComponent.floorTexture)
        {
            textures.Add(mapComponent.floorTexture);
            Retro3D.SetFloorTexture(renderer, (uint)(textures.Count - 1));
        }

        for (int iTexture = 0; iTexture < textures.Count; iTexture++)
        {
            Texture2D texture = textures[iTexture];
            if (texture == null)
                continue;
            Color32[] pixels = texture.GetPixels32();
            byte[] textureBytes = new byte[pixels.Length * 4];
            for (int iPixel = 0; iPixel < pixels.Length; iPixel++)
            {
                int byteIndex = iPixel * 4;
                textureBytes[byteIndex] = pixels[iPixel].r;
                textureBytes[byteIndex+1] = pixels[iPixel].g;
                textureBytes[byteIndex+2] = pixels[iPixel].b;
                textureBytes[byteIndex+3] = pixels[iPixel].a;
            }
            IntPtr textureBytesPtr = Marshal.UnsafeAddrOfPinnedArrayElement(textureBytes, 0);
            Retro3D.SetTexture(renderer, (uint)iTexture, textureBytesPtr, (uint)textureBytes.Length, (uint)texture.width, (uint)texture.height);
        }

        int dimX = mapComponent.dimX;
        int dimY = mapComponent.dimY;
        Int16[] map = new Int16[mapComponent.map.Length];
        for (int i = 0; i < mapComponent.map.Length; i++)
            map[i] = (Int16)(mapComponent.map[i]);

        IntPtr mapPtr = Marshal.UnsafeAddrOfPinnedArrayElement(map, 0);
        Retro3D.SetMap(renderer, mapPtr, (uint)dimX, (uint)dimY);

        Camera.onPostRender += OnPostRenderCallback;
    }

    void Update()
    {
        Vector2 forward = new Vector2(transform.forward.x, transform.forward.z).normalized;
        Retro3D.SetCamera(renderer, transform.position.x, transform.position.z, forward.x, forward.y);

        Retro3D.Update(renderer, Time.deltaTime);

        uint frameBufferSize = Retro3D.GetFrameBufferSize(renderer);
        IntPtr frameBufferPtr = Retro3D.GetFrameBufferPtr(renderer);
        byte[] frameBufferBytes = new byte[frameBufferSize];
        Marshal.Copy(frameBufferPtr, frameBufferBytes, 0, frameBufferBytes.Length);
        
        Color32[] colours = new Color32[FRAMEBUFFER_WIDTH * FRAMEBUFFER_HEIGHT];
        for (int iy = 0; iy < FRAMEBUFFER_HEIGHT; iy++)
        {
            for (int ix = 0; ix < FRAMEBUFFER_WIDTH; ix++)
            {
                int index = ix + iy * FRAMEBUFFER_WIDTH;
                colours[index].r = frameBufferBytes[index * 4];
                colours[index].g = frameBufferBytes[index * 4 + 1];
                colours[index].b = frameBufferBytes[index * 4 + 2];
                colours[index].a = 255;
            }
        }
        cameraTexture.SetPixels32(colours);
        cameraTexture.Apply();
        
    }

    void OnDestroy()
    {
        Camera.onPostRender -= OnPostRenderCallback;
    }

    void OnPostRenderCallback(Camera camera)
    {
        if (renderer != null)
        {
            //camera.targetTexture = null;
            //Graphics.Blit(cameraTexture, null as RenderTexture);
            //Graphics.Blit(cameraTexture, camera.targetTexture);
        }
    }

    void OnRenderImage(RenderTexture src, RenderTexture dest)
    {
        Graphics.Blit(cameraTexture, dest);
    }
}
