# Retro3D-Unity

WIP

Making a pseudo-3D software rendering plugin for Unity.

The project uses [my Retro3D C++ rendering library](https://codeberg.org/matiaslavik/Retro3D).

![](Screenshots/Screenshot1.png)
